﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace PrintMultipleLinesWithMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 25; i++)
            {
                helloworld();
                Thread.Sleep(50);
            }
        }

        static void helloworld()
        {
            Console.WriteLine("Hello World!");
        }

    }
}
